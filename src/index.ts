import { app, BrowserWindow, ipcMain, screen } from 'electron';
import { getFontFeatures, getFonts } from './utils/fonts';
import Store from 'electron-store';
// This allows TypeScript to pick up the magic constant that's auto-generated by Forge's Webpack
// plugin that tells the Electron app where to look for the Webpack-bundled app code (depending on
// whether you're running in development or production).
declare const MAIN_WINDOW_WEBPACK_ENTRY: string;
declare const MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY: string;

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
	// eslint-disable-line global-require
	app.quit();
}

let mainWindow: BrowserWindow;

const createWindow = (): void => {
	const { width, height } = screen.getPrimaryDisplay().workAreaSize;
	// Create the browser window.
	mainWindow = new BrowserWindow({
		height,
		width,
		webPreferences: {
			nodeIntegration: false,
			contextIsolation: true,
			preload: MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY,
		},
	});

	// and load the index.html of the app.
	mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);

	// Open the DevTools.
	mainWindow.webContents.openDevTools();
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	// On OS X it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (BrowserWindow.getAllWindows().length === 0) {
		createWindow();
	}
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
ipcMain.on('get-fonts', async () => {
	const fonts = await getFonts();
	mainWindow.webContents.send('fonts', fonts);
	const fontFeatures = fonts
		.map(font => {
			const features = Object.values(font.files).flatMap(file => {
				return getFontFeatures(file);
			});
			return {
				family: font.family,
				features: [...new Set(features)],
			};
		})
		.reduce(
			(previous, current) => ({
				...previous,
				[current.family]: current.features,
			}),
			{},
		);
	mainWindow.webContents.send('font-features', fontFeatures);
});

const store = new Store();

ipcMain.on('store', (e, { key, data }) => {
	store.set(key, data);
});

ipcMain.handle('get', (e, { key, defaultValue }) => {
	return store.get(key, defaultValue);
});
