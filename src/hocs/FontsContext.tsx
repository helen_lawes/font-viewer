import { SelectChangeEvent } from '@mui/material';
import React, {
	createContext,
	ReactNode,
	useContext,
	useEffect,
	useMemo,
	useState,
} from 'react';
import { findArrayInArray } from '../utils/fontFeatures';
import { FontFeatures, SystemFont, View } from '../utils/types';

interface FontsContext {
	fonts: SystemFont[];
	fontFeatures: FontFeatures;
	previewText: string;
	onPreviewTextChange?: (newText: string) => void;
	filterFeatures: string[];
	onFeatureSelectChange?: (e: SelectChangeEvent<string[]>) => void;
	addToFavourites?: (fontFamily: string) => void;
	removeFromFavourites?: (fontFamily: string) => void;
	isFavourite?: (fontFamily: string) => boolean;
	isCurrentView?: (view: View) => boolean;
	changeView?: (view: View) => void;
}

const defaultState: FontsContext = {
	fonts: [],
	fontFeatures: {},
	previewText: 'The quick brown fox jumped over the lazy dog',
	filterFeatures: [],
};

const FontsContext = createContext<FontsContext>(defaultState);

export const useFonts = (): FontsContext => useContext(FontsContext);

interface FontsProviderProps {
	children: ReactNode;
}

export const FontsProvider = ({
	children,
}: FontsProviderProps): JSX.Element => {
	const [fontsInfo, setFontsInfo] = useState(defaultState.fonts);
	const [previewText, setPreviewText] = useState(defaultState.previewText);
	const [fontFeatures, setFontFeatures] = useState(defaultState.fontFeatures);
	const [filterFeatures, setFilterFeatures] = useState<string[]>(
		defaultState.filterFeatures,
	);
	const [view, setView] = useState(View.main);
	const [favourites, setFavourites] = useState<string[]>([]);

	const fullInfo = useMemo(() => {
		return fontsInfo.map(({ family, ...other }) => ({
			...other,
			family,
			features: fontFeatures[family],
		}));
	}, [fontsInfo, fontFeatures]);

	const fonts = useMemo(() => {
		let filteredFonts = fullInfo;

		if (view === View.favourites) {
			filteredFonts = fullInfo.filter(({ family }) =>
				favourites.includes(family),
			);
		}

		if (!filterFeatures.length) {
			return filteredFonts;
		} else {
			return filteredFonts.filter(({ features }) => {
				return findArrayInArray(features, filterFeatures);
			});
		}
	}, [fontsInfo, filterFeatures, view, favourites]);

	const onPreviewTextChange = (newText: string) => {
		setPreviewText(newText);
	};

	const onFeatureSelectChange = (event: SelectChangeEvent<string[]>) => {
		const {
			target: { value },
		} = event;
		setFilterFeatures(
			// On autofill we get a stringified value.
			typeof value === 'string' ? value.split(',') : value,
		);
	};

	const storeFavourites = (newFavourites: string[]) => {
		api.store(View.favourites, newFavourites);
		setFavourites(newFavourites);
	};

	const addToFavourites = (fontFamily: string) => {
		const newFavourites = [...favourites, fontFamily];
		console.log(newFavourites);
		storeFavourites(newFavourites);
	};

	const removeFromFavourites = (fontFamily: string) => {
		const newFavourites = favourites.filter(
			family => family !== fontFamily,
		);
		console.log(newFavourites);
		storeFavourites(newFavourites);
	};

	const isFavourite = (fontFamily: string) => {
		return favourites.includes(fontFamily);
	};

	const isCurrentView = (localView: View) => {
		return view === localView;
	};

	const changeView = (newView: View) => {
		console.log('changeView');
		setView(newView);
	};

	useEffect(() => {
		api.send('get-fonts');
		api.receive('fonts', (fontList: SystemFont[]) => {
			setFontsInfo(fontList);
		});
		api.receive('font-features', (fontFeatures: FontFeatures) => {
			setFontFeatures(fontFeatures);
		});
		const getFavourites = async () => {
			const defaultFavourites = (await api.get(
				View.favourites,
				[],
			)) as string[];
			console.log('defaultFavourites', defaultFavourites);
			setFavourites(defaultFavourites);
		};
		getFavourites();
	}, []);

	return (
		<FontsContext.Provider
			value={{
				fonts,
				fontFeatures,
				previewText,
				onPreviewTextChange,
				filterFeatures,
				onFeatureSelectChange,
				addToFavourites,
				removeFromFavourites,
				isFavourite,
				isCurrentView,
				changeView,
			}}>
			{children}
		</FontsContext.Provider>
	);
};
