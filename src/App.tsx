import React from 'react';
import FontList from './components/FontList/FontList';
import Toolbar from './components/Toolbar/Toolbar';
import Sidebar from './components/Sidebar/Sidebar';
import { GlobalStyle, Wrapper } from './App.styles';
import { FontsProvider } from './hocs/FontsContext';

const App = (): JSX.Element => {
	return (
		<FontsProvider>
			<Wrapper>
				<GlobalStyle />
				<Sidebar />
				<Toolbar />
				<FontList />
			</Wrapper>
		</FontsProvider>
	);
};

export default App;
