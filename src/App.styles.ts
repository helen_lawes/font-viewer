import styled, { createGlobalStyle } from 'styled-components';
import { rem } from './utils/styles';

export const GlobalStyle = createGlobalStyle`
	body {
		font-family: "Roboto","Helvetica","Arial",sans-serif;
		padding: 0;
		margin: 0;
	}
	* {
		box-sizing: border-box;
	}
`;

export const Wrapper = styled.div`
	display: grid;
	grid-template-columns: ${rem(200)} auto;
	grid-template-rows: auto auto;
	grid-template-areas: 'sidebar toolbar' 'sidebar main';
`;
