declare module 'system-font-families';

interface API {
	send: (channel: string, data?: unknown) => void;
	receive: (channel: string, func: (...args: unknown[]) => void) => void;
	store: (key: string, data: unknown) => void;
	get: (key: string, defaultValue?: unknown) => Promise<unknown>;
}

declare const api: API;
