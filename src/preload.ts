import { ipcRenderer, contextBridge } from 'electron';

const validChannels = ['get-fonts', 'fonts', 'font-features'];
contextBridge.exposeInMainWorld('api', {
	send: (channel: string, data: unknown) => {
		// whitelist channels
		if (validChannels.includes(channel)) {
			ipcRenderer.send(channel, data);
		}
	},
	receive: (channel: string, func: (...args: unknown[]) => void) => {
		if (validChannels.includes(channel)) {
			// Deliberately strip event as it includes `sender`
			ipcRenderer.on(channel, (event, ...args) => func(...args));
		}
	},
	store: (key: string, data: unknown) => {
		ipcRenderer.send('store', {
			key,
			data,
		});
	},
	get: (key: string, defaultValue?: unknown) => {
		return ipcRenderer.invoke('get', { key, defaultValue });
	},
});
