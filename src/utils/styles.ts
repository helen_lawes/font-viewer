export const rem = (pixels: number, basePx = 16): string =>
	`${pixels / basePx}rem`;
