export interface SystemFont {
	family: string;
	files: { [key: string]: string };
	subFamilies: string[];
	features?: string[];
}

export interface FontFeatures {
	[key: string]: string[];
}

export interface FontFeatureType {
	name: string;
	value: string;
	regex?: RegExp;
}

export enum View {
	main = 'main',
	favourites = 'favourites',
}
