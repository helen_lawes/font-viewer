import SystemFonts from 'system-font-families';
import fontkit from 'fontkit';
import { SystemFont } from './types';

const systemFonts = new SystemFonts();

export const getFonts = async (): Promise<SystemFont[]> => {
	return systemFonts.getFontsExtended();
};

export const getFontFeatures = (file: string): string[] => {
	const meta = fontkit.openSync(file);
	return meta.availableFeatures;
};
