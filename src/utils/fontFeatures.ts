import { FontFeatureType } from './types';

export const fontFeatureTypes: FontFeatureType[] = [
	{
		name: 'All alternatives',
		value: 'aalt',
	},
	{
		name: 'Swash',
		value: 'swsh',
	},
	{
		name: 'Contextual swash',
		value: 'cwsh',
	},
	{
		name: 'Contextual alternatives',
		value: 'calt',
	},
	{
		name: 'Historial forms',
		value: 'hist',
	},
	{
		name: 'Localized forms',
		value: 'locl',
	},
	{
		name: 'Stylistic alternatives',
		value: 'salt',
	},
	{
		name: 'Stylistic set',
		value: 'ss',
		regex: /ss([0-9]{1,2})/,
	},
	{
		name: 'Subscript',
		value: 'subs',
	},
	{
		name: 'Superscript',
		value: 'sups',
	},
	{
		name: 'Standard ligatures',
		value: 'liga',
	},
];

const fontFeatureTypesObject: { [key: string]: FontFeatureType } =
	fontFeatureTypes.reduce(
		(previous, current) => ({
			...previous,
			[current.value]: current,
		}),
		{},
	);

export const findArrayInArray = (target: string[], find: string[]): boolean => {
	const regexes = find.map(f => {
		const feature = fontFeatureTypesObject[f];
		return feature.regex || new RegExp(feature.value);
	});
	return target.some(t => {
		return regexes.some(regex => regex.test(t));
	});
};
