import {
	IconButton,
	ListItem,
	ListItemText,
	Menu,
	MenuItem,
} from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import React, { useMemo, useState } from 'react';
import Font from '../Font/Font';
import { SystemFont } from '../../utils/types';
import FontDialog from '../Font/FontDialog';
import { useFonts } from '../../hocs/FontsContext';

interface FontListItemProps {
	font: SystemFont;
	previewText: string;
	containerId: string;
}

const potentialFontIssue = (subfamily: string[], fontName: string) => {
	const regular = subfamily.includes('Regular');
	const bold = subfamily.includes('Bold');
	const italic = subfamily.includes('Italic');
	const medium = subfamily.includes('Medium');
	const normal = subfamily.includes('Normal');
	const light = subfamily.includes('Light');
	const name = subfamily.includes(fontName);
	return !(regular || bold || italic || medium || normal || light || name);
};

const FontListItem = ({
	font,
	previewText,
	containerId,
}: FontListItemProps): JSX.Element => {
	const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
	const [detailsOpen, setDetailsOpen] = useState(false);
	const { addToFavourites, removeFromFavourites, isFavourite } = useFonts();
	const open = Boolean(anchorEl);
	const favourite = isFavourite(font.family);

	const fontIssue = useMemo(() => {
		return potentialFontIssue(font.subFamilies, font.family);
	}, [font]);

	const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	const openDetails = () => {
		handleClose();
		setDetailsOpen(true);
	};

	const closeDetails = () => {
		setDetailsOpen(false);
	};

	const add = () => {
		handleClose();
		addToFavourites?.(font.family);
	};

	const remove = () => {
		handleClose();
		removeFromFavourites?.(font.family);
	};

	return (
		<ListItem
			secondaryAction={
				<IconButton onClick={handleClick}>
					<MoreVertIcon />
				</IconButton>
			}
			sx={{ borderBottom: '1px solid rgba(0,0,0,0.12)' }}>
			<ListItemText
				primary={
					<Font
						font={font}
						fontIssue={fontIssue}
						previewText={previewText}
						containerId={`#${containerId}`}
					/>
				}
			/>
			<Menu anchorEl={anchorEl} open={open} onClose={handleClose}>
				<MenuItem onClick={openDetails}>View details</MenuItem>
				{!favourite && (
					<MenuItem onClick={add}>Add to favourites</MenuItem>
				)}
				{favourite && (
					<MenuItem onClick={remove}>Remove from favourites</MenuItem>
				)}
			</Menu>
			<FontDialog
				open={detailsOpen}
				onClose={closeDetails}
				previewText={previewText}
				font={font}
				fontIssue={fontIssue}
			/>
		</ListItem>
	);
};

export default FontListItem;
