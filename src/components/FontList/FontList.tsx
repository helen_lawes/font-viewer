import { List } from '@mui/material';
import React from 'react';
import { useFonts } from '../../hocs/FontsContext';
import { Wrapper } from './FontList.styles';
import FontListItem from './FontListItem';

const containerId = 'fontlist';

const FontList = (): JSX.Element => {
	const { fonts, previewText } = useFonts();
	return (
		<Wrapper id={containerId}>
			<List>
				{fonts.map((font, i) => (
					<FontListItem
						key={i}
						font={font}
						previewText={previewText}
						containerId={containerId}
					/>
				))}
			</List>
		</Wrapper>
	);
};

export default FontList;
