import { rem } from '../../utils/styles';
import styled from 'styled-components';

export const Wrapper = styled.div`
	grid-area: main;
	height: calc(100vh - ${rem(80)});
	overflow-y: scroll;
	padding: ${rem(10)} ${rem(15)};
`;
