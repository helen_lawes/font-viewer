import { rem } from '../../utils/styles';
import styled from 'styled-components';

export const DialogPreview = styled.div`
	font-size: ${rem(30)};
`;

export const AltName = styled.div`
	font-size: ${rem(12)};
`;

export const AltHeading = styled.h3`
	font-size: ${rem(20)};
	margin: ${rem(40)} 0 0;
`;
