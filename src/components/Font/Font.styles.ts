import { rem } from '../../utils/styles';
import styled from 'styled-components';

export const Wrapper = styled.div`
	padding: ${rem(10)} 0;
`;

export const FontName = styled.div`
	font-size: ${rem(12)};
`;

export const Preview = styled.div`
	font-size: ${rem(30)};
`;
