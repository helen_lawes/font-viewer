import {
	Dialog,
	DialogContent,
	DialogTitle,
	IconButton,
	List,
	ListItem,
	ListItemText,
	Typography,
} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import WarningIcon from '@mui/icons-material/Warning';
import React from 'react';
import { SystemFont } from '../../utils/types';
import { DialogPreview, AltName, AltHeading } from './FontDialog.styles';

interface FontDialogProps {
	open: boolean;
	onClose: () => void;
	previewText: string;
	font: SystemFont;
	fontIssue: boolean;
}

const FontDialog = ({
	open,
	onClose,
	previewText,
	font,
	fontIssue,
}: FontDialogProps): JSX.Element => {
	return (
		<Dialog open={open} onClose={onClose}>
			<DialogTitle
				sx={{
					display: 'flex',
					justifyContent: 'space-between',
					alignContent: 'center',
					alignItems: 'center',
				}}>
				Font details - {font.family}{' '}
				{fontIssue && <WarningIcon sx={{ fontSize: 16 }} />}
				<IconButton
					aria-label="close"
					onClick={onClose}
					sx={{ padding: 0 }}>
					<CloseIcon />
				</IconButton>
			</DialogTitle>
			<DialogContent dividers>
				<Typography variant="body1" component="div">
					<DialogPreview
						style={{
							fontFamily: font.family,
							fontFeatureSettings: 'normal',
						}}>
						{previewText}
					</DialogPreview>
					{font.features && font.features.length > 0 && (
						<>
							<AltHeading>Font alternatives</AltHeading>
							<List>
								{font.features.map(feature => (
									<ListItem key={feature} disableGutters>
										<ListItemText
											primary={
												<>
													<AltName>{feature}</AltName>
													<DialogPreview
														style={{
															fontFamily:
																font.family,
															fontFeatureSettings: `"${feature}"`,
														}}>
														{previewText}
													</DialogPreview>
												</>
											}
										/>
									</ListItem>
								))}
							</List>
						</>
					)}
				</Typography>
			</DialogContent>
		</Dialog>
	);
};

export default FontDialog;
