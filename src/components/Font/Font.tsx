import React from 'react';
import LazyLoad from 'react-lazyload';
import WarningIcon from '@mui/icons-material/Warning';
import { SystemFont } from '../../utils/types';
import { Wrapper, FontName, Preview } from './Font.styles';

interface FontProps {
	font: SystemFont;
	fontIssue: boolean;
	previewText: string;
	containerId: string;
}

const Font = ({
	font,
	fontIssue,
	previewText,
	containerId,
}: FontProps): JSX.Element => {
	return (
		<Wrapper>
			<FontName>
				{font.family}{' '}
				{fontIssue && <WarningIcon sx={{ fontSize: 16 }} />}
			</FontName>
			<LazyLoad height={40} scrollContainer={containerId}>
				<Preview
					style={{
						fontFamily: font.family,
						fontFeatureSettings: 'normal',
					}}>
					{previewText || font.family}
				</Preview>
			</LazyLoad>
		</Wrapper>
	);
};

export default Font;
