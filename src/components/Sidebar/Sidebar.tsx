import {
	Divider,
	List,
	ListItemButton,
	ListItemIcon,
	ListItemText,
} from '@mui/material';
import FavoriteIcon from '@mui/icons-material/Favorite';
import AddIcon from '@mui/icons-material/Add';
import HomeIcon from '@mui/icons-material/Home';
import React from 'react';
import { Wrapper } from './Sidebar.styles';
import { useFonts } from '../../hocs/FontsContext';
import { View } from '../../utils/types';

const Sidebar = (): JSX.Element => {
	const { isCurrentView, changeView } = useFonts();
	const changeSelectedView = (view: View) => () => {
		changeView(view);
	};
	return (
		<Wrapper>
			<List>
				<ListItemButton
					selected={isCurrentView(View.main)}
					onClick={changeSelectedView(View.main)}>
					<ListItemIcon>
						<HomeIcon />
					</ListItemIcon>
					<ListItemText primary="All" />
				</ListItemButton>
				<ListItemButton
					selected={isCurrentView(View.favourites)}
					onClick={changeSelectedView(View.favourites)}>
					<ListItemIcon>
						<FavoriteIcon />
					</ListItemIcon>
					<ListItemText primary="Favourites" />
				</ListItemButton>
			</List>
			<Divider />
			<List>
				<ListItemButton>
					<ListItemIcon>
						<AddIcon />
					</ListItemIcon>
					<ListItemText primary="Add category" />
				</ListItemButton>
			</List>
		</Wrapper>
	);
};

export default Sidebar;
