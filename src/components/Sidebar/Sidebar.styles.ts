import styled from 'styled-components';

export const Wrapper = styled.div`
	grid-area: sidebar;
	box-shadow: 2px 0 5px #c0c0c0;
`;
