import { rem } from '../../utils/styles';
import styled from 'styled-components';

export const Wrapper = styled.div`
	grid-area: toolbar;
	display: flex;
	column-gap: ${rem(10)};
	height: ${rem(80)};
	box-shadow: 3px 2px 5px #c0c0c0;
	background: #fefefe;
	padding: ${rem(10)} ${rem(15)};
	border: 1px solid #c0c0c0;
`;
