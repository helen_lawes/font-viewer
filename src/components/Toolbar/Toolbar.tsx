import {
	FormControl,
	InputLabel,
	MenuItem,
	Select,
	TextField,
} from '@mui/material';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { Wrapper } from './Toolbar.styles';
import { useFonts } from '../../hocs/FontsContext';
import { fontFeatureTypes } from '../../utils/fontFeatures';
import { useDebounce } from '../../hooks/useDebounce';

const Toolbar = (): JSX.Element => {
	const {
		previewText,
		onPreviewTextChange,
		filterFeatures,
		onFeatureSelectChange,
	} = useFonts();
	const [text, setText] = useState<string>(previewText);
	const debouncedText = useDebounce(text, 300);
	const onTextChange = (e: ChangeEvent<HTMLInputElement>) => {
		setText(e.target.value);
	};

	useEffect(() => {
		onPreviewTextChange(debouncedText);
	}, [debouncedText]);

	return (
		<Wrapper>
			<TextField
				label="Preview text"
				value={text}
				onChange={onTextChange}
				variant="filled"
				sx={{ width: 300 }}
			/>
			<FormControl variant="filled" sx={{ width: 150 }}>
				<InputLabel id="font-features-label">Font features</InputLabel>
				<Select
					labelId="font-features-label"
					value={filterFeatures}
					onChange={onFeatureSelectChange}
					multiple>
					{fontFeatureTypes.map((feature, i) => (
						<MenuItem key={i} value={feature.value}>
							{feature.name}
						</MenuItem>
					))}
				</Select>
			</FormControl>
		</Wrapper>
	);
};

export default Toolbar;
